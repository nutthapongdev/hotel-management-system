class Guest {
  String? name;
  int? age;

  Guest({this.name, this.age});

  @override
  String toString() {
    // TODO: implement toString
    return "name : $name - age : $age";
  }
}
