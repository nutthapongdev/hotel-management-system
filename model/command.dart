class Command {
  String? name;
  List<String>? params;

  Command(this.name, this.params);
}