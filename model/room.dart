import 'guest.dart';

class Room {
  int? floor;
  int? room;
  int? keyCard;
  Guest? guest;

  Room({this.floor, this.room, this.guest, this.keyCard});

  String numberRoom() {
    return "$floor${room.toString().length == 1 ? "0$room" : room}";
  }

  @override
  String toString() {
    // TODO: implement toString
    return "$floor${room.toString().length == 1 ? "0$room" : room} keyCard => $keyCard";
  }
}

