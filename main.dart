import 'dart:developer';
import 'dart:io';

import 'constants/constants.dart';
import 'constants/enum.dart';
import 'model/command.dart';
import 'model/guest.dart';
import 'model/room.dart';

void main() {
  const filename = Constants.inputFileName;
  final file = File(filename);
  final results = file.readAsLinesSync();

  List<Room> listRoom = [];
  List<Guest> listGuest = [];
  List<int> keyCards = [];

  void createHotel({required Command command}) {
    try {
      int floor = int.parse(command.params?[0] ?? "0");
      int room = int.parse(command.params?[1] ?? "0");
      int numberKeyCard = 1;

      for (int i = 1; i <= floor; i++) {
        for (int j = 1; j <= room; j++) {
          Room room = Room(
            floor: i,
            room: j,
          );
          listRoom.add(room);
          keyCards.add(numberKeyCard++);
        }
      }

      log("Hotel created with $floor floor(s), $room room(s) per floor.");
    } catch (e) {
      throw e;
    }
  }

  void book({required Command command}) {
    try {
      String numberRoom = command.params?[0] ?? "";
      Guest guest = Guest(name: command.params?[1], age: int.parse(command.params?[2] ?? "0"));

      bool isCheckGuestDuplicate = listGuest.any((element) => element.name == guest.name && element.age == guest.age);

      if (!isCheckGuestDuplicate) {
        listGuest.add(guest);
      }

      listRoom.forEach((e) {
        if (e.numberRoom() == numberRoom) {
          if (e.guest == null) {
            e.keyCard = keyCards.first;
            keyCards.removeAt(0);
            log("Room ${e.numberRoom()} is booked by ${guest.name} with keycard number ${e.keyCard}.");
            e.guest = guest;
          } else {
            log("Cannot book room ${e.numberRoom()} for ${guest.name}, The room is currently booked by ${e.guest?.name}.");
          }
        }
      });
    } catch (e) {
      throw e;
    }
  }

  void getListAvailableRooms() {
    try {
      String listAvailableRooms = "";
      listRoom.forEach((element) {
        if (element.guest == null) {
          listAvailableRooms += listAvailableRooms.isEmpty ? element.numberRoom() : ", ${element.numberRoom()}";
        }
      });

      log(listAvailableRooms);
    } catch (e) {
      throw e;
    }
  }

  void checkout({required Command command}) {
    try {
      String keyCard = command.params?[0] ?? "";
      String nameGuest = command.params?[1] ?? "";
      String room = "";
      bool checkKeyCard = false;

      checkKeyCard = listRoom.any((e) => e.keyCard == int.parse(keyCard) && e.guest?.name == nameGuest);

      if (checkKeyCard) {
        listRoom.forEach((e) {
          if (e.keyCard == int.parse(keyCard) && e.guest?.name == nameGuest) {
            keyCards.add(e.keyCard ?? 0);
            keyCards.sort();
            room = e.numberRoom();
            e.guest = null;
            e.keyCard = null;
            log("Room $room is checkout.");
          }
        });
      } else {
        log("Only Thor can checkout with keycard number $keyCard.");
      }
    } catch (e) {
      throw e;
    }
  }

  void getlistGuest() {
    try {
      List<Guest> guestInHotel = [];

      listGuest.forEach((element) {
        bool isCheckGuestInHotel = listRoom.any((e) => (e.guest?.name?.contains(element.name ?? "") ?? false) && e.guest?.age == element.age);
        if (isCheckGuestInHotel) {
          guestInHotel.add(element);
        }
      });

      listGuest = guestInHotel;

      String listGuestInHotel = "";
      listGuest.forEach((element) {
        listGuestInHotel += listGuestInHotel.isEmpty ? element.name ?? "" : ", ${element.name}";
      });

      log(listGuestInHotel);
    } catch (e) {
      throw e;
    }
  }

  void getGuestInRoom({required Command command}) {
    try {
      String room = command.params?[0] ?? "";

      String guestName = listRoom.firstWhere((element) => element.numberRoom() == room).guest?.name ?? "";

      log(guestName);
    } catch (e) {
      throw e;
    }
  }

  void getGuestByAge({required Command command}) {
    try {
      String operator = command.params?[0] ?? "";
      int age = int.parse(command.params?[1] ?? "");

      String listGuestByAge = "";
      listGuest.forEach((element) {
        switch (operator) {
          case "=":
            if ((element.age ?? 0) == age) {
              listGuestByAge += listGuestByAge.isEmpty ? element.name ?? "" : ", ${element.name}";
            }
            break;
          case ">":
            if ((element.age ?? 0) > age) {
              listGuestByAge += listGuestByAge.isEmpty ? element.name ?? "" : ", ${element.name}";
            }
            break;
          case "<":
            if ((element.age ?? 0) < age) {
              listGuestByAge += listGuestByAge.isEmpty ? element.name ?? "" : ", ${element.name}";
            }
            break;
        }
      });

      log(listGuestByAge);
    } catch (e) {
      throw e;
    }
  }

  void getGuestByFloor({required Command command}) {
    try {
      String floor = command.params?[0] ?? "";

      String listGuestByFloor = "";

      listRoom.forEach((element) {
        if (element.floor == int.parse(floor)) {
          listGuestByFloor += listGuestByFloor.isEmpty ? element.guest?.name ?? "" : ", ${element.guest?.name}";
        }
      });

      log(listGuestByFloor);
    } catch (e) {
      throw e;
    }
  }

  void checkoutGuestByFloor({required Command command}) {
    try {
      String floor = command.params?[0] ?? "";

      List<Room> roomwithcustomers = [];

      listRoom.forEach((element) {
        if (element.guest != null && element.floor == int.parse(floor)) {
          roomwithcustomers.add(element);
        }
      });

      String roomCheckOut = "";

      roomwithcustomers.forEach(((element) {
        roomCheckOut += roomCheckOut.isEmpty ? element.numberRoom() : ", ${element.numberRoom()}";

        listRoom.forEach((e) {
          if (e.numberRoom() == element.numberRoom()) {
            keyCards.add(e.keyCard ?? 0);
            keyCards.sort();
            e.guest = null;
            e.keyCard = null;
          }
        });
      }));

      log("Room $roomCheckOut are checkout.");
    } catch (e) {
      throw e;
    }
  }

  void bookByFloor({required Command command}) {
    try {
      List<Room> roomEmptyByFloor = [];

      String floor = command.params?[0] ?? "";

      String roomCheckIn = "";

      String keyCardsRoom = "";

      Guest guest = Guest(name: command.params?[1], age: int.parse(command.params?[2] ?? "0"));

      listRoom.forEach((element) {
        if (element.guest == null && element.floor == int.parse(floor)) {
          roomEmptyByFloor.add(element);
        }
      });

      bool isCheckAllRoomEmptyOnthefloor = roomEmptyByFloor.length == listRoom.where((element) => element.floor == int.parse(floor)).toList().length;

      if (isCheckAllRoomEmptyOnthefloor) {
        bool isCheckGuestDuplicate = listGuest.any((element) => element.name == guest.name && element.age == guest.age);

        if (!isCheckGuestDuplicate) {
          listGuest.add(guest);
        }
        roomEmptyByFloor.forEach((element) {
          listRoom.forEach((e) {
            if (e.numberRoom() == element.numberRoom()) {
              roomCheckIn += roomCheckIn.isEmpty ? element.numberRoom() : ", ${element.numberRoom()}";
              element.keyCard = keyCards.first;
              keyCards.removeAt(0);
              element.guest = guest;
              keyCardsRoom += keyCardsRoom.isEmpty ? element.keyCard.toString() : ", ${element.keyCard}";
            }
          });
        });

        log("Room $roomCheckIn are booked with keycard number $keyCardsRoom");
      } else {
        log("Cannot book floor $floor for ${guest.name}.");
      }
    } catch (e) {
      throw e;
    }
  }

  results.forEach((e) {
    final command = Command(e.split(" ")[0], e.split(" ").sublist(1));

    CommandFunction nameCommand = CommandFunction.values.byName(command.name ?? "");

    switch (nameCommand) {
      case CommandFunction.create_hotel:
        // TODO: Handle this case.
        createHotel(command: command);

        break;
      case CommandFunction.book:
        // TODO: Handle this case.
        book(command: command);

        break;
      case CommandFunction.list_available_rooms:
        // TODO: Handle this case.
        getListAvailableRooms();

        break;
      case CommandFunction.checkout:
        // TODO: Handle this case.
        checkout(command: command);

        break;
      case CommandFunction.list_guest:
        // TODO: Handle this case.
        getlistGuest();

        break;
      case CommandFunction.get_guest_in_room:
        // TODO: Handle this case.
        getGuestInRoom(command: command);

        break;
      case CommandFunction.list_guest_by_age:
        // TODO: Handle this case.
        getGuestByAge(command: command);

        break;
      case CommandFunction.list_guest_by_floor:
        // TODO: Handle this case.
        getGuestByFloor(command: command);

        break;
      case CommandFunction.checkout_guest_by_floor:
        // TODO: Handle this case.
        checkoutGuestByFloor(command: command);

        break;
      case CommandFunction.book_by_floor:
        // TODO: Handle this case.
        bookByFloor(command: command);

        break;
      default:
        log("No Command");
    }
  });
}
